#!/bin/sh

# PREPARATION
## Install dependencies
sudo apt update
sudo apt install python-pip python-dev python-setuptools python-virtualenv git libyaml-dev build-essential virtualenv -y

# INSTALLATION
## create virtualenv and install octoprint
sudo virtualenv /opt/OctoPrint
sudo /opt/OctoPrint/bin/pip install OctoPrint

## add user to correct groups
sudo usermod -a -G tty $USER
sudo usermod -a -G dialout $USER

# AUTOMATIC STARTUP
## get dependencies
wget https://github.com/foosel/OctoPrint/raw/master/scripts/octoprint.init && sudo mv octoprint.init /etc/init.d/octoprint
wget https://github.com/foosel/OctoPrint/raw/master/scripts/octoprint.default && sudo mv octoprint.default /etc/default/octoprint
sudo chmod +x /etc/init.d/octoprint

## copy config file
sed -i "s/OCTOPRINT_USER/OCTOPRINT_USER=${USER}/g" octoprint
sed -i "s/BASEDIR/BASEDIR=\/home\/${USER}\/.octoprint/g" octoprint
sudo cp ./octoprint /etc/default/octoprint

## update service and restart
sudo update-rc.d octoprint defaults
sudo service octoprint restart

# MAKE ACCESSIBLE AT PORT 80
## Install haproxy
sudo apt install haproxy -y

## Copy config file
sudo cp ./haproxy.cfg /etc/haproxy/haproxy.cfg

## restart service
sudo service haproxy restart

# SHUTDOWN PERMISSION
## add file to sudoers to allow shutdown without root paswd
echo "please add '${USER} ALL=NOPASSWD: /sbin/shutdown' to file: /etc/sudoers.d/octoprint-shutdown"
